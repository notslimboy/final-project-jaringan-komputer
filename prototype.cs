
// quick count client 
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 7412);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;

            while (true)
            {
                Console.Write("Enter your answer: ");
                s = Console.ReadLine();
                Console.WriteLine();
                writer.WriteLine(s);
                writer.Flush();


            }
            reader.Close();
            writer.Close();
            client.Close();

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    } // end Main()
} // end class definition