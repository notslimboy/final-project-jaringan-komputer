// quick count client 
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

public class client
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = "";
            String J = "";
            var quit = false;

            Console.WriteLine("=================* CLIENT *================");
            Console.WriteLine("=================* Connected *================");

            Console.Write("input your username: ");
            String uname = String.Empty;
            uname = Console.ReadLine();
            writer.WriteLine(uname);
            writer.Flush();

            Console.WriteLine("");
            Console.WriteLine("WELCOME " + uname + ", Good Luck!");
            Console.WriteLine("");

            while (!quit)
            {
                try
                {
                    
                    s = reader.ReadLine();
                    
                    Console.WriteLine(s);

                    string inputAnswer;
                    int answer;
                    Console.WriteLine("\nEnter your answer: ");
                    inputAnswer = Console.ReadLine();

                    // Validate user input 
                    while (!Int32.TryParse(inputAnswer, out answer))
                    {
                        Console.WriteLine("Masukkan Angka, COBA LAGI ");
                        inputAnswer = Console.ReadLine();
                    }

                  
                    writer.WriteLine(inputAnswer);
                    writer.Flush();

                    s = reader.ReadLine();
                  
                    Console.WriteLine(s);
                    
                }
                catch (System.IO.IOException e)
                {
                   
                    reader.Close();
                    writer.Close();
                    client.Close();
                    quit = true;
                }

              
            }




        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    } // end Main()
} // end class definition