// quick count server 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        public static TcpClient client = null;
        public static TcpClient clientOpponent = null;
        public static int counter = 0;
        public void Math(object argument)
        {
            StreamReader reader = null;
            StreamWriter writer = null;
            counter++;
            Console.WriteLine("{0}", counter);
            int gameCounter = counter;
            Console.WriteLine("Game Counter : {0}", gameCounter);
            if (gameCounter == 1)
                client =  (TcpClient)argument;
            if (gameCounter == 2)
                clientOpponent =  (TcpClient)argument;
            
            String user = String.Empty;
            try
            {
                if (gameCounter == 1)
                {
                    reader = new StreamReader(client.GetStream());
                    writer = new StreamWriter(client.GetStream());
                }
                if (gameCounter == 2)
                {
                    reader = new StreamReader(clientOpponent.GetStream());
                    writer = new StreamWriter(clientOpponent.GetStream());
                }

                user = reader.ReadLine();
                Console.WriteLine("Username Client: " + user);
                int sum;
                string operatorString;
                var quit = false;
                int score = 0;


                while (!quit)
                {
                    if (score == 1000)
                    {
                        writer.WriteLine("Selamat anda menang !!! ");
                        writer.Flush();
                        quit = true;
                        break;
                    }


                    // generate random  number 
                    Random rnd = new Random();
                    var operation = rnd.Next(1, 4);
                    var num1 = rnd.Next(1, 151);
                    var num2 = rnd.Next(1, 151);

                    switch (operation)
                    {
                        case 1:
                            sum = num1 + num2;
                            operatorString = "+";
                            break;

                        case 2:
                            sum = num1 - num2;
                            operatorString = "-";
                            break;

                        case 3:
                            sum = num1 * num2;
                            operatorString = "*";
                            break;

                        default:
                            sum = 0;
                            operatorString = " ";
                            break;

                    }

                    Console.WriteLine("{0} {1} {2}  = ", num1, operatorString, num2);
                    writer.WriteLine("{0} {1} {2}  = ", num1, operatorString, num2);
                    writer.Flush();

                    string recv = reader.ReadLine();
                    int answer = Convert.ToInt32(recv);


                 

                    if (sum == answer)
                    {
                        score = score + 100;
                        Console.WriteLine("user:" + user + " = " + score);
                        writer.WriteLine("Benar. ");
                        writer.Flush();
                    }

                    else
                    {
                        if ( score == 0)
                        { 
                           score = 0;
                        }
                        else
                        {
                            score = score - 100;
                        }
                        Console.WriteLine("user:" + user + " = " + score);
                        writer.WriteLine("Salah. ");
                        writer.Flush();
                    }

                }

                reader.Close();
                writer.Close();
                client.Close();
                clientOpponent.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Problem with client communication. Exiting thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                    
                }
            }
        }




        public static void Main()
        {
            Program game = new Program();
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();
                Console.WriteLine("Quick Count Server started...");
                while (true)
                {
                    Console.WriteLine("Waiting for incoming client connections...");
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Accepted new client connection...");
                    Thread t = new Thread(game.Math);
                    t.Start(client);
                    
                 


                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}